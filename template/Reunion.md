Ceci est un modèle de prise de notes pour une réunion. Le modèle est modifiable/supprimable dans /Framaspace/Modèle/Réunion.md  
Rappel : l'enregistrement est automatique, la prise de notes peut être collaborative. Vous pouvez afficher les "couleurs par auteur" en cliquant sur l’icône "Personnes actives" (à droite), puis "Afficher les couleurs par auteur" Vous pouvez supprimer les lignes ci-dessus, et éditer les lignes ci-dessus comme bon vous semble.

# Réunion « Jardin partagé »

📆 11 décembre 2022 🕛 13h30

👥 Pierre, Camille, Fred

💻 Lien de visio :

## Temps d'accueil

Quelques minutes pour prendre la "météo" de chaque participant⋅es (sans prise de notes)

## Ressources des précédentes réunions

Voir le dossier /Dossier partagé/Projet/Jardin/Réunions

## Ordre du jour

* Point sur la subvention
* Nettoyage du jardin
* Qui pour ajouter l'événement du 24/12 sur <https://mobilizon.fr> ?

## Prise de notes

(à faire)

## Qui fait quoi, quand ?

✅ Fred s'occupe de relancer la maire d'ici le 15/12

✅ Camille mobilise 2 bénévoles pour nettoyer le jardin demain

✅ Pierre ajoute l'événement sur Mobilizon le 12/12