Bienvenue dans l'application "Fichiers", qui constitue le cœur de votre espace Frama.space !

L'icône "+" ci-dessus vous permettra d'ajouter des dossiers ou fichiers. L'icône "Partager" permet de définir les options de partage.

* [Documentation utilisateur⋅ice](https://documentation.coopaname.coop/2.%20G%C3%A9rer%20ses%20documents%20en%20ligne/Readme.html) (chez les ami⋅es de Coopaname, mais le fonctionnement est le même sur Frama.space)
* Besoin d'aide ? Des questions ? Posez-les sur le [forum d'entraide Frama.space](https://forum.frama.space/c/comment-faire-pour/gerer-partager-des-fichiers/12).
* Vous voulez modifier (ou supprimer) ce texte ? Modifiez (ou supprimez) le fichier "Readme.md" ci-dessous.