Le dossier "Collectifs" contient les documentations collectives écrite par vous ou collectivement.

Pour comprendre comment cela fonctionne, vous pouvez lire <https://forum.frama.space/t/debuter-avec-lapplication-collectifs/112/2>

**Il est déconseillé de supprimer ce dossier.**