do
$$
declare
    tos record;
begin
    for tos in SELECT id FROM oc_termsofservice_terms
    loop
        INSERT INTO oc_termsofservice_sigs (terms_id, user_id, timestamp)
        VALUES (tos.id, 'admin', extract(epoch from now()));
    end loop;
end;
$$;
