Bonjour !

L’association Framasoft met gratuitement à disposition cet espace collaboratif pour votre association/collectif.

Cependant, comme il s’agit d’un espace où vous allez pouvoir déposer des fichiers, travailler collaborativement, partager des photos, etc, il y a certaines règles à respecter.

Pour faire simple, c’est un peu comme si nous vous mettions à disposition un logement dans un village. Ce que vous faites dans ce logement ne nous regarde pas. Mais si vous abusez de cette mise à disposition gratuite, alors nous vous demanderons de partir.
C’est pourquoi nous vous demandons d’accepter ces règles de « bonne gestion d’utilisation du commun Frama.space » avant de pouvoir l’utiliser.

Vous trouverez, ci-dessous et en termes compréhensibles par des humain⋅es, les conditions d’utilisations de notre service. Les versions complètes et détaillées, que vous vous engagez à accepter sont accessibles à la fin de ce document.

# Conditions Générales d’Utilisation des services Framasoft

On est sympathiques :

* Votre contenu vous appartient (mais nous vous encourageons à le publier sous licence libre) ;
* Framasoft n’exploitera pas vos données personnelles, sauf à fin de statistiques internes (anonymisées) ou pour vous prévenir d’un changement important sur le service ;
* Framasoft ne transmettra ni ne revendra vos données personnelles (votre vie privée nous tient — vraiment — à cœur) ;
* Les engagements de Framasoft résultent d’une démarche militante inspirée du mouvement de l’éducation populaire, vous pouvez les retrouver dans notre [Charte des services Libres, Ethiques, Décentralisés et Solidaires](https://framasoft.org/fr/charte/).

Mais il ne faut pas nous prendre pour autant pour des chatons :

* Clause « La loi est la loi, et on ne veut pas finir en taule » : vous devriez respecter la loi (que celle-ci soit bien faite ou idiote), sinon votre compte pourrait être supprimé ;
* Clause « Merci d’être poli·e et respectueux·se avec ses voisin·es de service » : vous devez respecter les autres utilisateur·ices en faisant preuve de civisme et de politesse, sinon votre contenu, voire votre compte, pourront être supprimés, sans négociation ;
* Clause « Si ça casse, on n’est pas obligé de réparer » : Framasoft propose ce service gratuitement et librement. Si vous perdez des données, par votre faute ou par la nôtre, désolé, mais ça arrive. Nous ferons ce que nous pourrons pour les récupérer, mais nous ne nous assignons aucune obligation de résultat. En clair, évitez de mettre des données sensibles ou importantes sur les services Framasoft, car en cas de perte, nous ne garantissons pas leur récupération ;
* Clause « Si vous n’êtes pas content·es, vous êtes invité·es à aller voir ailleurs » : si le service ne vous convient pas, libre à vous d’en trouver un équivalent (ou meilleur) ailleurs, ou de monter le vôtre ;
* Clause « Tout abus sera puni » : si un·e utilisateur·ice abuse du service, par exemple en monopolisant des ressources machines partagées, ou en publiant des contenus considérés comme non pertinents, son contenu ou son compte pourra être supprimé sans avertissement ni négociation. Framasoft reste seul juge de cette notion « d’abus » dans le but de fournir le meilleur service possible à l’ensemble de ses utilisateur·ices. Si cela vous parait anti-démocratique, anti-libriste, anti-liberté-d’expression, merci de vous référer à la clause précédente ;
* Clause « Rien n’est éternel » : les services peuvent fermer (faute de fonds pour les maintenir, par exemple), ils peuvent être victimes d’intrusion (le « 100% sécurisé » n’existe pas). Nous vous encourageons donc à conserver une copie des données qui vous importent, car Framasoft ne saurait être tenu pour responsable de leur hébergement sans limite de temps.

# Conditions Spécifiques d’Utilisation du service Frama.space

* **Framasoft est hébergeur technique** des machines physiques opérant le service Frama.space, ainsi que prestataire technique du service Frama.space.
* En conséquence, Framasoft n’est « que » le sous-traitant d’hébergement des données à caractères personnelles qui pourraient être hébergées sur les espaces Frama.space.
* En conséquence, Framasoft ne peut être reconnu comme responsable de traitement de ces données. D’ailleurs Framasoft s’engage à ne pas regarder/manipuler/exploiter vos données, sauf pour motif de gestion technique du service, ou en cas de défaillance du Responsable de Traitement.
* **Le Responsable de Traitement est la personne qui a souscrit au service Frama.space** (c’est à dire l’administrateur du compte Nextcloud/Frama.space), ou la personne mandatée publiquement par ce dernier
  * Le Responsable de Traitement devra s’assurer que l’ensemble des utilisateurs de l’espace Frama.space dont il a la responsabilité sont correctement informés de leurs droits et devoirs concernant la gestion des données à caractère personnel.
  * Il est rappelé que l’hébergeur des machines physiques est la société européenne, de droit allemand, Hetzner, et que cette dernière n’a pas de certification pour l’hébergement des données de santé.
  * Il lui incombe de faire respecter l’exercice des droits (accès, modification, suppression, etc) des utilisateurs dont il a la responsabilité.
  * Les demandes d’exercices de ces droits doivent se faire auprès du Responsable de Traitement.
    * Si ce dernier ne réagit pas, l’utilisateur pourra s’adresser à Framasoft.

La liste des données collectées, et l’usage qui en est fait, peut être consulté sur la page « [Vie privée et RGPD](https://www.frama.space/abc/fr/rgpd/) »


# Vie privée et RGPD
Framasoft s’engage à respecter vos données, personnelles comme non personnelles.

Nous nous efforçons de minimiser le nombre de données personnelles recueillies, tout simplement parce que, au delà même de la loi ou du Règlement Général sur la Protection des Données (RGPD), nous n’en tirons aucune utilité.

En effet, Framasoft s’engage à ne faire aucune utilisation de vos données, hors besoins statistiques de base ou besoins techniques légitimes. C’est à dire que nous ne revendons pas vos données, et nous nous fichons éperdument de savoir qui vous êtes ou de valoriser vos profils de données ou vos contenus. C’est même l’un des sujets au cœur de notre association : démontrer qu’il est tout à fait possible d’opérer des services en ligne, sans chercher à profiter des informations communiquées par nos utilisateur⋅ices.

La contrepartie de cette confiance, c’est que nous vous demandons, lorsque VOUS créez des contenus à l’aide de nos outils et services, de prêter attention à ne pas stocker ou divulguer d’informations personnelles sans obtenir de consentement préalable et explicite. Formulé différemment : si vous devenez utilisateur ou utilisatrice de la plateforme Frama.space, vous vous engagez à respecter le cadre fixé par le RGPD.

# Conclusion
En validant ces conditions d’utilisation, vous confirmez avoir lu et accepté sans réserve les trois textes ci-dessous :

* [Conditions Générales d’Utilisation](https://framasoft.org/fr/cgu/),
* [Conditions Spécifiques d’Utilisation du service Frama.space](https://www.frama.space/abc/csu/), 
* [Vie privée et RGPD](https://www.frama.space/abc/rgpd/)
