# Demeter

This repository contains the skeleton files to provide to each new Frama.space user.

# License

* Illustrations: CC-BY-SA [David Revoy](https://www.davidrevoy.com/)
